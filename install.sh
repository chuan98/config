#!/usr/bin/env bash

git clone https://gitee.com/pikka9/config.git ~/.config
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting
